# Learn MkDocs

MkDocs는 프로젝트 문서 작성을 위한 **빠르고 단순하며 보기 좋은** 정적 사이트 생성기이다. Markdown으로 문서 파일을 작성하고, YAML 형식 파일로 설정한다. [입문 튜토리얼](getting-started.md)로 시작하며, 자세한 정보는 [사용자 지침서](userguide/installation.md)에 기술되어 있다.
