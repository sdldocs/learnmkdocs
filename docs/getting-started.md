# MkDocs 시작하기 

## 설치
아래와 같이 명령어를 실행하여 MkDocs를 설치한다.
```
pip install mkdocs
```

[설치 안내 문서](userguide/installation.md)에 보다 자세히 기술되어 있다.

## 새 프로젝트 생성
쉽게 시작할 수 있다. 아래 명령을 수행하여 새 프로젝트 문서 작성을 시작할 수 있다.
```
mkdocs new my-project
cd my-project
```

방금 생성한 <code>my-project</code> 프로젝트 폴더는 아래 그림과 같이 구성되어 있다.
![my-project-folder](images/fig-01.png)
설정 파일 <code>mkdocs.yml</code>이 있으며, 문서 파일을 저장할 <code>docs</code> 폴더가 보인다 (<code>docs</code>는 환경 설정 변수 <code>docs_dir</code>의 디폴트 값이다). 또한 <code>mkdocs new my-project</code> 명령으로 <code>docs</code> 폴더에 문서 페이지 파일 <code>index.md</code>가 저장되어 있다.

MkDocs는 작성 중인 문서에 대한 미리 보기 기능이 있는 개발 서버 (div-server)를 내장하고 있다. <code>mkdocs.yml</code>이 있는 <code>my-project</code> 폴더에서 <code>mkdocs serve</code> 명령으로 서버를 실행시킨다.

![run-mkdocs-serve](images/fig-02.png)

브라우서에서 <code>http://127.0.0.1:8000/</code>를 열면 아래와 같은 페이지를 볼 수 있다.
![first_page](images/fig-03.png)

div-server는 자동 재로드 (auto-reloading) 기능을 지원하여, 설정 파일, 문서 폴더, 테마 폴더가 업데이트 될 때마다 문서를 다시 생성한다.

편집기로 <code>docs/index.md</code> 파일을 열어, 원 제목을 <code>MkLorum</code>로 변경하여 저장한다. 그러면 브라우저에 재동 재로드되어 변경된 문서를 즉시 볼 수 있다.

이제 설정 파일 <code>mkdocs.yml</code>를 편집하여 보자. <code>site_name</code>의 설정을 <code>MkLorum</code>로 변경하고 저장한다.
![mkdocs.yml](images/fig-04.png)

즉시 브라우저에 다시 로드하여 아래 그림과 같이 변경된 결과를 볼 수 있다.
![mkdocs.yml-browser](images/fig-05.png)

Note: 
설정 파일에서 <code>site_name</code>과 <code>site_url</code>은 필수 옵션이다. 프로젝트를 새로이 시작하면, <code>site_url</code>이 <code>https://example.com</code>으로 설정된다. 만약 사용할 url을 알면, 이 주소를 변경할 수 있다. 또는 이때 변경하지 않고, 배포 직전에 이를 변경하도록 할 수 있다.  

## 페이지 추가
프로젝트에 두번째 페이지를 추가하여 보자.

```
curl 'https://jaspervdj.be/lorem-markdownum/markdown.txt' > docs/about.md
```

우리 문서 사이트에는 네비게이션 헤더가 보일 수 있도록 설정 파일을 편집하여 <code>nav</code>를 설정을 추가함으로써 나타나는 순서, 제목과 네비게이션 헤더가  페이지를 포함하도록 할 수 있다.
![edit-mkdocs.yml](images/fig-06.png)

변경된 </code>mkdocs.yml</code>을 저장하고, 화면의 왼쪽에 <code>Home</code>과 <code>About</code>과 오른쪽에 <code>Search</code>, <code>Previous</code>와 <code>Next</code>를 네비게이션 바가 나타난다.
![result-mkdocs.yml](images/fig-07.png)

메뉴 항목들을 클릭하여 페이지들을 네비게이트하여 보고, <code>Search</code>를 클릭하여 보면 페이지에서 용어를 탐새할 수 있는 탐색 창이 아래 그림과 같이 pop up 된다. 검색 결과로 사이트의 모든 검색어가 포함되며 검색어가 나타나는 페이지로 직접 연결할 수 있다. 추가 노력이나 구성 없이 검색 기능을 지원할 수 있다!
![search-popup](images/fig-08.png)

## 문서 테마 지정
테마 설정을 변경하여 문서의 시각적 표현을 변화시키기 위하여 설정 퍼일을 변경할 수 있다. 이를 위하여 <code>mkdocs.yml</code>를 편집하여 <code>theme</code> 설정을 추가한다.

![change-theme](images/fig-09.png)

이를 저장하고, <code>mkdocs serve</code>를 실행하면 아래 그림과 같은 변경한 ReadTheDocs 테마를 사용한 페이지를 볼 수 있다.

![change-theme-result](images/fig-10.png)

## Favicon 아이콘 변경
기본적으로 MkDocs는 [MkDocs favicon](https://www.mkdocs.org/img/favicon.ico) 아이콘을 사용합니다. 다른 아이콘을 사용하려면 docs 디렉토리의 하위 디렉토리 <code>img</code>에 생성하고 사용자 정의 <code>favicon.ico</code> 파일을 해당 폴더에 복사한다. MkDocs는 해당 파일을 자동으로 검색하여 사용자의 즐겨찾기 아이콘으로 사용한다.

## 사이트 구축
OK! <code>MkLorum</code> 문서를 배포할 준비가 되었다. 먼저 문서를 생성한다.
```
mkdocs build
```

위의 명령을 실행하면 <code>site</code>라는 새 디렉토리를 만들 것이다. 그 디렉토리에는 내용은 아래와 같다.
![after_build](images/fig-11.png)

소스 문서로 부터 <code>index.html</code>과 <code>about/index.html</code> 두 HTML 파일을 출력하였더. 또한 문서 테마의 한 부분으로 <code>site</code> 디렉토리에 다양한 미디어 파일들이 복사되었다. 또한, <code>sitemap.xml</code>과 <code>mkdocs/search_index.json</code>도 생성되었음을 알 수 있다.

<code>git</code>과 같은 소스 코드 관리 도구를 사용하고 있다면, 문서 생성을 위한 리포지토리(repository)를 제외할 수 있다. 이를 위하여 <code>.gitignore</code>에 <code>site/</code>를 포함하도록 아래 명령을 수행한다.
```
echo "site/" >> .gitignore
```

다른 소스 코드 관리 도구를 사용하는 경우 특정 디렉터리를 제외하는 방법에 대한 설명서를 확인해야 한다.

## 기타 명령과 옵션
다양한 명령과 옵션을 사용할 수 있다. 이에 대한 리스트는 아래와 같은 명령을 사용하여 볼 수 있다.
```
mkdocs --help
```

특정한 명령에 사용할 수 있는 옵션만을 볼 수 있다. 이를 위하여 명령에 <code>--help</code> 플래그를 사용한다. 예를 들머, <code>build</code> 명령에서 사용할 수 있는 옵션을 모두 알고 싶다면, 다음 명령을 수행하면 된다.
```
mkdocs build --help
```

## 배포
방금 만든 문서 사이트는 정적 파일만 사용하므로 어느 웹서버도 호스팅할 수 있다. 전체 사이트 디렉토리의 내용을 웹 사이트를 호스팅하는 위치에 업로드하기만 하면 되기 때문이다. [문서 배포 페이지](userguide/deploying-your-docs.md)에서 다양한 공용 호스트에서의 구체적인 방법을 설명하고 있다.

## 도움이 필요할 때
[사용자 지침서](userguide/index.md)에서 Mkdocs의 모든 기능에 대한 보다 자세한 설명을 찾을 수 있다.

[GitHub discussions](https://github.com/mkdocs/mkdocs/discussions) 및 [GitHub issues](https://github.com/mkdocs/mkdocs/issues)에서 MkDocs에 대한 도움을 받을 수도 있다.
