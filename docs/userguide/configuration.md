# 설정

## 소개 <a id="introduction"></a>
프로젝트 설정은 기본적으로 <code>mkdocs.yml</code>라는 프로젝트 디렉터리에 있는 YAML 설정 파일을 사용한다. <code>-f/--config-file</code> 옵션으로 다른 경로를 지정할 수 있다 (<code>mkdocs build --help</code> 참조).

이 설정 파일에는 최소한 <code>site_name</code>과  <챙ㄷ>site_url</챙ㄷ> 설정이 포함되어야 한다. 다른 모든 설정은 선택 사항이다.

## 프로젝트 정보 <a id="project_information"></a>

### site_name <a id="site_name"></a>
**필수 설정**이며 문자열로 프로젝트 문서의 주 제목으로 사용된다. 예:
```
site_name: Marshmallow Generator
```

테마를 렌더링할 때 이 설정은 <code>site_name</code> 컨텍스트 변수로 전달된다.

### site_url <a id="site_url"></a>
사이트의 기준 URL을 설정한다. 그러면 <code>기준</code> URL을 각 HTML 페이지의 <code>헤드</code> 섹션의 <code>링크</code> 태그에 추가한다. MkDocs 사이트의 'root'가 도메인의 하위 디렉터리 내에 있을 경우 해당 하위 디렉터리(<code>https://example.com/foo/</code>)로 설정해야 한다.

이 설정은 <code>mkdocs serve</code> 명령에도 사용된다. 서버를 URL의 경로 구설 요소에서 가져온 경로로 마운트한다. 예를 들어, <code>some/page.md</code>를 <code>http:///127.0.0.1:8000/foo/some/page</code>/에서 원격 레이아웃을 모방하여 서비스한다.

**default**: <code>null</code>

### repo_url <a id="repo_url"></a>
설정하면 각 페이지의 리포지토리(GitHub, Bitbucket, GitLab 등)에 대한 링크를 제공한다.

```
repo_url: https://github.com/example/repository/
```

**default**: <code>null</code>

### repo_name <a id="repo_name"></a>
설정하면 각 페이지의 리포지토리에 대한 링크의 이름을 제공한다.

**default**: <code>repo_url</code>이 해당 도메인과 일치하면 <code>'GitHub'</code>, <code>'Bitbucket'</code> 또는 <code>'GitLab'</code>이 되고, 그렇지 않으면 <code>repo_url</code>의 호스트 이름이 된다.

### edit_url <a id="edit_url"></a>
페이지를 직접 볼 때 기준 <code>repo_url</code>에서 docs 디렉토리까지의 경로이다. 리포지토리 호스트(예: GitHub, Bitbucket 등), 분기 및 docs 디렉토리 자체의 세부 사항을 고려한다. MkDocs는 <code>repo_url</code>과 <code>edit_uri</code>를 연결하고 페이지의 입력 경로를 추가한다.

설정되고 테마가 지원하는 이를 경우, 소스 리포지토리의 페이지 링크를 직접 제공한다. 이렇게 하면 페이지 소스을 더 쉽게 찾고 편집할 수 있다. <code>repo_url</code>이 설정되지 않았다면 이 옵션은 무시된다. 일부 테마에서 이 옵션을 설정하면 리포지토리 링크 대신 편집 링크로 사용될 수 있다. 다른 테마는 두 링크를 모두 표시할 수 있다.

<code>edit_uri</code>는 쿼리 (?')와 프레그멘트 ('#') 문자를 지원한다. 쿼리 또는 프레그멘트을 사용하여 파일에 액세스하는 리포지토리 호스트의 경우 <code>edit_uri</code>를 다음과 같이 설정할 수 있다. (URI의 <code>?</code>와 <code>#</code>에 유의한다.)

```
# Query string example
edit_uri: '?query=root/path/docs/'
```

```
# Hash fragment example
edit_uri: '#root/path/docs/'
```

다른 리포지토리 호스트의 경우 docs 디렉토리에 대한 상대 경로를 지정하기만 하면 된다.

```
# Query string example
edit_uri: root/path/docs/
```

> **Note**: <br>
> 몇몇 알려진 호스트 (특히 GitHub, BitBucket 및 GitLab)에서는 <code>edit_uri</code>가 'repo_url'에서 파생되므로 수동으로 설정할 필요가 없다. <code>repo_url</code>을 정의하기만 하면 자동으로 <code>edit_uri</code>를 설정한다.

> 예를 들어 GitHub- 또는 GitLab 호스트 리포지토리의 경우 <code>edit_uri</code>를 <code>edit/master/docs/</code>로 자동 설정한다 (<code>eidt</code> 경로 및 <code>master</code> 분기 참고).

> Bitbucket 호스트 리포지토리의 경우 동등한 <code>edit_uri</code>을 <code>src/default/docs/</code>로 자동으로 설정한다 (<code>src</code> 경로 및 <code>default</code> 분기 참조).

> 기본값과 다른 URI(예: 다른 분기)를 사용하려면 <code>edit_uri</code>를 원하는 문자열로 설정한다. 페이지에 "URL edit link"를 표시하지 않으려면 <code>edit_uri</code>를 빈 문자열로 설정하여 자동 설정을 비활성화할 수 있다.

> **Warning** <br>
> GitHub과 GitLab에서 온라인 편집기는 기본 "edit" 경로(<code>edit/master/docs/</code>)로 페이지를 엽니다. 이 기능을 사용하려면 사용자가 GitHub/GitLab에 계정을 가지고 있어야 한다. 그렇지 않으면 로그인/가입 페이지로 리디렉션된다. 또는 "blob" 경로(<code>blob/master/docs/</code>)를 사용하여 익명 액세스를 지원하는 읽기 전용 보기를 사용할 수 있다.

**default**: <code>repo_url</code>이 해당 도메인과 일치하면, GitHub 및 GitLab 리포지토리의 경우 <code>edit/master/docs/</code> 또는 Bitbucket 리포지토리의 경우 <code>src/default/docs/</code>이며,  그렇지 않으면 <code>null</code> 이다.

### site_description <a id="site_description"></a>
사이트 설명을 설정한다. 설정으로부터 메타 태그를 생성하여 HTML 헤더에 추가된다.

**default**: <code>null</code>

### site_author <a id="site_author"></a>
사이트 저작자을 설정한다. 설정으로부터 메타 태그를 생성하여 HTML 헤더에 추가된다.

**default**: <code>null</code>

### copyright <a id="copyright"></a>
문서에 포함할 저작권 정보를 테마별로 설정한다.

**default**: <code>null</code>

### remote_branch <a id="remote_branch"></a>
GitHub Pages에 배포하기 위하여 gh-deploy를 사용하는 경우 커밋할 원격 분기를 설정한다. 이 옵션은 <code>gh-deploy</code> 명령어 옵션으로 오버라이드할 수 있다.

**default**: <code>gh-pages</code>

### remote_name <a id="remote_name"></a>
GitHub Pages에 배포하기 위하여 gh-deploy를 사용하는 경우 푸시할 원격 이름을 설정한다. 이 옵션은 <code>gh-deploy</code> 명령어 옵션으로 오버라이드할 수 있다.

**default**: <code>origin</code>

## 문서 레이아웃 <a id="documentation-layout"></a>

### nav <a id="nav"></a>
이 설정은 사이트에 대한 전역 네비게이션의 형식과 레이아웃을 결정한다. 최소 탐색 구성은 다음과 같다.

```
nav:
    - 'index.md'
    - 'about.md'
```

탐색 구성의 모든 경로는 <code>docs_dir</code> 설정에 상대적이어야 한다. [페이지와 탐색 설정](/userguide/writing-your-docs/#configure-pages-and-navigation)에서 하위 섹션 작성 방법을 포함한 자세한 설명하고 있다. 

탐색 항목에 외부 사이트에 대한 링크도 포함할 수 있다. 내부 링크의 경우 제목이 선택 사항이지만 외부 링크의 경우 제목이 필요하다. 외부 링크는 전체 URL 또는 상대 URL일 수도 있다. 파일에서 찾을 수 없는 모든 경로는 외부 링크로 간주한다. [메터데이터](/userguide/writing-your-docs/#meta-data)에서 MkDocs이 문서의 페이지 제목을 결정하는 방법을 설명하고 있다. 

```
nav:
    - Introduction: 'index.md'
    - 'about.md'
    - 'Issue Tracker': 'https://example.com/'
```

위의 예에서 처음 두 항목은 로컬 파일을 가리키고 세 번째 항목은 외부 사이트를 가리키고 있다.

그러나 MkDocs 사이트가 프로젝트 사이트의 하위 디렉터리에서 호스트되는 경우가 있으므로 전체 도메인을 포함하지 않고 동일한 사이트의 다른 부분에 링크할 수 있다. 이 경우 적절한 상대 URL을 사용할 수 있다.

```
site_url: https://example.com/foo/

nav:
    - Home: '../'
    - 'User Guide': 'user-guide.md'
    - 'Bug Tracker': '/bugs/'
```

위의 예에서 두 가지 다른 스타일의 외부 링크를 사용하고 있다. 먼저 <code>site_url</code>은 MkDocs 사이트를 도메인의 /foo/ 하위 디렉토리에서 호스트함을 나타내고 있다. 따라서 <code>Home</code> 탐색 항목은 서버 루트로 한 단계 상위 단계임을 효과적으로 <code>https://example.com/</code>을 가리키는 상대 링크이다. <code>Bug Tracker</code> 항목은 서버 루트의 절대 경로를 사용하며 효과적으로 <code>https://example.com/bugs/</code>을 가리킨다. 물론 <code>User Guide</code>는 로컬 MkDocs 페이지를 가리키고 있다.

**default**: 기본적으로 <code>nav</code>는 <code>docs_dir</code> 및 하위 디렉터리에 있는 모든 Markdown 파일의 알파벳순으로 정렬되고 중첩된 목록을 포함한다. 하위 섹션에서 인덱스 파일은 항상 먼저 나타난다.

## 빌드 디렉토리 <a id="build-directory"></a>

### 테마 <a id="theme"></a>
문서 사이트의 테마와 테마별 옵션을 설정한다. 설정 값은 문자열 또는 key/value 쌍 집합일 수 있다.

문자열인 경우 설치된 알려진 테마의 이름이어야 합니다. 사용 가능한 테마 목록을 보려면 [테마 선택](/userguide/choosing-your-theme/)으로 이동한다.

키/값 쌍 집합 예는 다음과 같다.

```
theme:
    name: mkdocs
    locale: en
    custom_dir: my_theme_customizations/
    static_templates:
        - sitemap.html
    include_sidebar: false
```

키/값 쌍 집합인 경우 다음과 같이 키를 정의할 수 있다.

name:
: 설치된 테마 이름이다. 사용 가능한 테마 목록을 [테마 선택](/userguide/choosing-your-theme/)에서 볼 수 있다.

locale:
: 사이트의 언어를 나타내는 코드이다. [테마 로칼라이징](/userguide/localizing-your-theme/)에서 상세히 설명하고 있다. 

custom_dir:
: 사용자 정의 테마가 있는 디렉터리이다. 설정 파일이 있는 디렉터리를 기준으로 상대 경로일 수도 있고 로컬 파일 시스템의 루트에서 시작하는 절대 디렉터리 경로일 수도 있다.
: [테마 사용자 정의](/userguide/customizing-your-theme)를 참조하여 기존 테마를 조정할 수 있다.
: [테마 개발자 가이드](https://www.mkdocs.org/dev-guide/themes/)를 참조하여 처음부터 자신만의 테마를 구축할 수 있다.

static_template:
: 정적 페이지로 렌더링할 템플릿 목록이다. 템플릿은 테마의 템플릿 디렉터리 또는 테마 설정에 정의된 <code>custom_dir</code>에 있어야 한다.

(theme specific keywords):
: 테마가 지원하는 추가 키워드도 정의할 수 있다. 자세한 내용은 사용 중인 테마의 설명서를 참조한다.

**default**: <code>'mkdocs'</code>

### docs_dir <a id="docs_dir"></a>
문서의 소스 마크다운 파일이 들어 있는 디렉터리이다. 이 디렉터리는 설정 파일이 들어 있는 디렉터리를 기준으로 상대 디렉터리 경로일 수도 있고 로컬 파일 시스템의 루트에서 시작되는 절대 디렉터리 경로일 수도 있다.

**default**: <code>'docs'</code>

### site_dir <a id="site_dir"></a>
출력 HTML 및 기타 파일이 생성되는 디렉토리이다. 이 디렉터리는 설정 파일이 들어 있는 디렉터리를 기준으로 상대 디렉터리 경로일 수도 있고 로컬 파일 시스템의 루트에서 시작되는 절대 디렉터리 경로일 수도 있다.

**default**: <code>'site'</code>

> **Note**: <br>
> 소스 코드 관리 도구를 사용하는 경우 빌드 출력 파일이 리포지토리에 커밋되지 않도록 하고 소스 파일만 버전 관리 하에 유지될 수 있도록 한다. 예를 들어, <code>git</code>를 사용하는 경우 <code>.gitignore</code> 파일에 다음 줄을 추가한다.

> ```
> site/
> ```

> 다른 소스 코드 관리 도구를 사용하는 경우 특정 디렉터리를 무시하는 방법에 대한 설명서를 확인해야 한다.

### extra_css <a id="extra_css"></a>
<code>docs_dir</code>에서 테마별로 포함할 CSS 파일 목록을 설정한다. 예를 들어, [docs_dir](#docs_dir)의 하위 디렉터리 css에  extra.css 파일을 포함할 수 있다.
```
extra_css:
    - css/extra.css
    - css/second_extra.css
```

**default**: <code>[]</code> (빈 리스트)

### extra_javascript <a id="extra_javascript"></a>
<code>docs_dir</code>에서 테마별로 포함할 JavaScript 파일 목록을 설정한다. 자세한 내용은 위의 [extra_css](#extra_css)의 사용을 참조한다.

**default**: <code>[]</code> (빈 리스트)

### extra_templates <a id="extra_templates">></a>
MkDocs에서 작성할 템플릿 목록을 <code>docs_dir</code>에 설정한다. MkDocs용 템플릿 작성에 대한 자세한 내용은 [테마 사용자 정의](/userguide/customizing-your-theme/)에 대한 설명과 템플릿에 대한 [사용 가능한 변수](?)에 대한 섹션을 참조하십시오. 자세한 내용은 [extra_css](#extra_css)의 사영을 참조한다.

**default**: <code>[]</code> (빈 리스트)

### extra <a id="extra"></a>
키-값 쌍의 집합을 템플릿에 전달한다. 이때 값은 YAML 구성요소로 유효하여야 한다. 이는 사용자 정의 테마를 만들 때 뛰어난 유연성을 제공한다.

예를 들어 프로젝트 버전 표시를 지원하는 테마를 사용하는 경우 다음과 같이 테마로 전달할 수 있다.
```
extra:
    version: 1.0
```

**default**: 기본적으로 <code>extra</code>는 빈 key-value 매팅이다.

## Preview controls <a id="preview-controls"></a>

### use_directory_urls <a id="use_directory_urls"></a>
이 설정은 문서내의 페이지를링킹에 사용되는 스타일을 제어한다.

다음 표는 <code>use_directory_urls</code>를 <code>true</code> 또는 <code>false</code>로 설정할 때 사이트에 사용되는 URL이 바뀌는 방법을 보여준다.

| **Source file** | **use_directory_url:true** | **use_directory_url:false** |
|-----------------|----------------------------|-----------------------------|
| index.md        | /                          | /index.html                 |
| api-guide.md    | /api-guide/                | /api-guide.html             |
|about/license.md | /about/license/            | /about/license.html         | 

<code>use_directory_urls</code>의 기본 스타일  보다 사용자 친화적인 URL을 생성하며 통산적으로 원하는 바이다.

다른 스타일은 대상 디렉토리가 아닌 대상 *파일*을 직접 가리키는 연결을 작성하므로 파일 시스템에서 직접 페이지를 열 때 설명서가 제대로 연결된 상태를 유지하려는 경우에 유용할 수 있다.

**default**: <code>true</code>

### strict <a id="strict"></a>
경고를 처리하는 방법을 결정한다. 경고가 발생할 때 처리를 중지하려면 <code>true</code>로 설정한다. 경고를 출력하고 처리를 계속하려면 <code>false</code>로 설정한다.

**default**: <code>false</code>

### dev_addr <a id="dev_addr"></a>
<code>mkdocs serve</code>를 실행할 때 사용되는 주소를 결정한다. <code>IP:PORT</code> 형식이어야 한다.

<code>mkdocs serve</code> 명령이 호출될 때마다 <code>--dev-addr</code> 옵션을 통해 전달할 필요 없이 사용자 기본값을 설정할 수 있다.

**default**: <code>'127.0.0.1:8000'</code>

[site-url](#site_url)을 참조한다.

## 포맷팅 옵션 <a id="formatting-options"></a>

### markdown_extensions <a id="markdown_extensions"></a>
MkDocs는 [Python Markdown](https://python-markdown.github.io/) 라이브러리를 사용하여 Markdwon 파일을 HTML로 변환한다. Python Markdwon은 팽지를 포맷하는 방싱을 커스터마이징하는 [확장](https://python-markdown.github.io/extensions/)을 지원한다. 이 설정을 사용하면 MkDocs가 기본적으로 사용하는 확장 목록(<code>meta</code>, <code>toc</code>, <code>tables</code>과 <code>fenced_code</code>)을 넘는 확장들을 활성화할 수 있다.

예를 들어 [SmartyPants typography extension]()를 다음과 같이 활성화시킨다.

```
markdown_extensions:
    - smarty
```

일부 확장은 자체 설정 옵션을 제공하기도 한다. 설정 옵션을 설정하려면 지정된 확장에서 지원하는 옵션의 키/값 매핑(<code>option_name: option value</code>)을 포함하면 된다. 지원되는 옵션을 확인하려면 사용 중인 확장의 설명서를 참조한다.

예를 들어, (포함된) <code>toc</code> 확장에서 permalink를 활성화하려면 다음과 같이 사용한다.
```
markdown_extensions:
    - toc:
        permalink: True
```

확장 이름(<code>toc</code>) 뒤에 콜론(<code>:</code>)이 와야 하며 새 줄에 옵션 이름과 값을 들여쓰고 콜론으로 구분해야 한다. 단일 확장에 대해 여러 옵션을 정의하려면 각 옵션을 별도의 줄에 정의해야 한다.
```
markdown_extensions:
    - toc:
        permalink: True
        separator: "_"
```

각 확장자 리스트에 항목을 추가한다. 특정 확장에 대해 설정할 옵션이 없다면 해당 확장에 대한 옵션을 생략하면 된다.
```
markdown_extensions:
    - smarty
    - toc:
        permalink: True
    - sane_lists
```

위의 예에서 각 확장자는 리스트 항목(<code>-</code>로 시작)이다. 대신 키/값 쌍을 사용할 수 있다. 그러나 이 경우 옵션을 정의하지 않은 확장에 대해도 빈 값을 제공해야 한다. 따라서 다음과 같이 위의 마지막 사례를 정의할 수 있다.
```
markdown_extensions:
    smarty: {}
    toc:
        permalink: True
    sane_lists: {}
```

[상속](#configuration-inheritance)을 통해 일부 옵션을 오버라이드하고자 할 떄 이 대체 구문이 필요하다.

> See Also <br>
> Python-Markdown 설명서는 즉시 사용할 수 있는 [확장 목록](https://python-markdown.github.io/extensions/)을 제공하고 있다. 지정된 확장에 사용할 수 있는 설정 옵션 목록은 해당 확장 설명서를 참조하여야 한다.

> 다양한 [서드 파티 확장](https://github.com/Python-Markdown/markdown/wiki/Third-Party-Extensions)을 설치하고 사용할 수도 있다. 설치 지침 및 사용 가능한 설치 옵션에 대해서는 해당 확장에서 제공하는 설명서를 참조하여야 한다.

**default**: <code>[]</code> (빈 리스트)

### plugins <a id="plugins"></a>
사이트를 구축할 때 사용할 플러그인 (선택 사항 설정과 같이) 목록에 대한 자세한 내용은 [플러그인](#plugins) 문서를 참조한다.

<code>플러그인</code> 설정이 <code>mkdocs.yml</code> 파일에 정의된 경우 모든 기본값(예: <code>search</code>)은 무시되며 기본값을 계속 사용하려면 기본값을 명시적으로 다시 실행해야 한다.
```
plugins:
    - search
    - your_other_plugin
```

주어진 플러그인에 옵션을 설정하려면 key/value 쌍들의 집합을 사용하여야 한다.
```
plugins:
    - search
    - your_other_plugin:
        option1: value
        option2: other value
```

위의 예에서 각 플러그인은 리스트의 항목입니다 (<code>-</code>로 시작). 대신 key/value 쌍을 사용할 수 있다. 그러나 이 경우 옵션이 정의되지 않은 플러그인에 대하여는 빈 값을 제공해야 한다. 따라서 위의 마지막 사례는 다음과 같이 설정할 수 있다.
```
plugins:
    search: {}
    your_other_plugin:
        option1: value
        option2: other value
```

[상속](#configuration-inheritance)을 통해 일부 옵션을 오버라이드하고자 할 떄 이 대체 구문이 필요하다.

기본값을 포함한 모든 플러그인을 사용하지 않으려면 플러그인 설정을 빈 리스트로 설정하여야 한다.
```
plugins: []
```

**default**: <code>['search]</code> (MkDocs에 포함된 "search" 플러그인)

### Search <a id="search"></a>
기본적으로 [lunr.js](https://lunrjs.com/)를 검색 엔진으로 사용하는 MkDocs은 검색 플러그인도 제공한다. 다음 설정 옵션을 사용하여 검색 플러그인의 작동을 변경할 수 있다.

#### sepaerator
색인을 구축할 때 단어간 구분 문자로 사용되는 문자를 찾는 정규 표현식(regular expression)이다. 기본적으로 공백과 하이픈(<code>-</code>)을 사용한다. 다음과 같이 점(<code>.</code>)을 단어 구분 문자로 추가할 수 있다.
```
plugins:
    - search:
        separator: '[\s\-\.]+'
```

**default**: <code>'[\s\-]+'</code> 

#### min_search-length
검색 질의어의 최소 길이를 정의하는 정수 값이다. 길이가 3자 미만인 검색어는 검색 결과가 품질이 떨어지므로 기본적으로 무시됩니다. 그러나 일부 사용 사례(예: 메시지 큐에 대한 문서를 찾기 위한 'MQ'에 대한 검색)에서는 더 짧은 제한을 설정할 수도 있다.
```
plugins:
    - search:
        min_search_length: 2
```

**default**: 3 

#### lang
[ISO 639-1] 언어 코드로 식별되는 검색 색인을 작성할 때 사용할 언어 목록이다. [Lunr Languages](https://github.com/MihaiValentin/lunr-languages#lunr-languages-----)에서는 다음 언어를 지원한다.

* <code>ar</code>: Arabic
* <code>da</code>: Danish
* <code>nl</code>: Dutch
* <code>en</code>: English
* <code>fi</code>: Finnish
* <code>fr</code>: French
* <code>de</code>: German
* <code>hu</code>: Hungarian
* <code>it</code>: Italian
* <code>ja</code>: Japanese
* <code>no</code>: Norwegian
* <code>pt</code>: Portuguese
* <code>ro</code>: Romanian
* <code>ru</code>: Russian
* <code>es</code>: Spanish
* <code>sv</code>: Swedish
* <code>th</code>: Thai
* <code>tr</code>: Turkish
* <code>vi</code>: Vietnamese

* <code>ko</code>: Korean (?)

[언어 추가에 기여](https://github.com/MihaiValentin/lunr-languages/blob/master/CONTRIBUTING.md)할 수 있다.

> Warning
> 검색에서 여러 언어를 동시에 지원할 수 있지만 실제로 필요하지 않은 경우 다른 언어를 추가하지 않는 것이 좋다. 언어를 추가할 때마다 상당한 대역폭이 필요하며 더 많은 브라우저 리소스를 사용한다. 일반적으로 MkDocs의 각 인스턴스를 단일 언어로 유지하는 것이 가장 좋다.

> Note
> 현재 Lunr Languages는 중국어 또는 기타 아시아 언어에 대한 지원을 하지 않고 있다. 그러나 일본어를 사용한 일부 사용자는 괜찮은 결과를 보고했다.

**default**: <code>theme.locale</code>의 설정 값, 그렇지 않으면 <code>[en]</code>.

#### prebuild_index
선택적으로 모든 페이지의 미리 작성된 인덱스를 생성하여 대형 사이트에 대한 성능 향상을 제공할 수 있다. 이를 활성화하기 전에 사용 중인 테마가 사전 빌드된 색인 사용을 지원하는지 확인해야 한다 (기본 제공 테마는 지원). <code>true</code>로 설정하여 활성화할 수 있다.

> Warning
> 이 옵션을 사용하려면 [Node.js](https://nodejs.org/)가 설치되어 있어야 하며 명령 <code>node</code>가 시스템 경로에 있어야 한다. 어떤 이유로든 <code>node</code> 실행이 실패하면 경고가 발생하지만 빌드는 중단 없이 계속된다. 빌드할 때 <code>--strict</code> 플래그를 사용하면 오류를 발생시킬 수 있다.

> Note
> 소규모 사이트에서는 대역폭 요구 사항이 크게 증가하므로 미리 작성된 인덱스를 사용하지 않는 것이 좋다. 그러나 대형 사이트(수백 페이지)의 경우 대역폭 증가가 상대적으로 적고 사용자 검색 성능이 크게 향상되었음을 알 수 있다.

**default**: <code>False</code> 

#### indexing
페이지의 색인을 구축할 때 검색 인덱서가 사용할 전략을 설정한다. 이 속성은 프로젝트 규모가 크고 인덱스가 디스크 공간을 많이 차지하는 경우에 특히 유용하다.
```
plugins:
    - search:
        indexing: 'full'
```

옵션

| **Option** | **Description** |
|------------|-----------------|
| <code>full</code> | 각 페이지의 제목, 섹션 제목 및 전체 텍스트를 색인한다. |
| <code>sections</code> | 각 페이지의 제목과 섹션 제목을 색인한다. |
| <code>titles</code> | 각 페이지의 제목만을 색인한다. |

**default**: <code>Full</code> 

## Environment Variables <a id="environment-variables"></a>
대부분의 경우 설정 옵션 값은 설정 파일에서 직접 설정된다. 그러나 선택으로 설정 옵션의 값을 <code>!ENV</code> 태그를 사용하여 환경 변수의 값으로 설정할 수 있다. 예를 들어 <code>site_name</code> 옵션 값을 <code>SITE_NAME</code> 변수 값으로 설정하려면 YAML 파일에 다음과 같이 선언되어 있어야 한다.

```
site_name: !ENV SITE_NAME
```

환경 변수가 정의되지 않은 경우 설정에 <code>null</code>(또는 Python에서의 <code>None</code>) 값을 할당한다. 다음과 같이 리스트의 마지막 값으로 기본값을 정의할 수 있다. 

```
site_name: !ENV [SITE_NAME, 'My default site name']
```

예비 변수도 여러 개 사용할 수 있다. 마지막 값은 환경 변수는 아니지만 지정된 환경 변수가 정의되지 않은 경우 기본값으로 사용할 수 있는 값이어야 한다.

```
site_name: !ENV [SITE_NAME, OTHER_NAME, 'My default site name']
```

문자열, bool, 정수, float, timestamp와 null과 같은 환경 변수 내에 정의된 기본 데이터 타입을 YAML 파일에 직접 정의된 것처럼 파싱되므로 값을 적절한 데이터 타입으로 변환한다. 그러나 목록 또는 키/값 쌍과 같은 복합 데이터 타입 (complex data type)은 단일 환경 변수를 이용하여 정의할 수 없다.

[pyyaml_env_tag](https://github.com/waylan/pyyaml-env-tag) 프로젝트에서 보다 자세한 사항을 찾아 볼 수 있다.

## Configuration Inheritance <a id="configuration-inheritance"></a>
일반적으로 하나의 파일로 사이트의 전체 설정을 저장한다. 그러나 일부 조직에서는 공통 구성을 공유하는 여러 사이트를 유지 관리할 수 있다. 각각 따로 설정을 유지 관리하기보다는 공통 설정 옵션을 부모 설정 파일에서 정의하고 각각 사이트에서는 주 설정 파일로부터 상속받아 설정에 사용할 수 있다.

설정 파일의 부모를 정의하려면 <code>INHERIT</code>(모두 대문자로 표시) 키를 부모 파일의 경로로 설정하여야 한다. 경로는 주 파일의 위치에 상대적이어야 한다.

설정 옵션을 부모 설정과 병합하려면 이러한 옵션을 키/값 쌍으로 정의해야 한다. 특히 [markdown_extensions](#markdown_extensions)과 [plugins](#plugins) 옵션은 리스트 항목(<code>-</code>로 시작하는 줄)이 아닌 다른 구문을 사용해야 한다.

예를 들어 공통(부모) 설정이 다음과 같이 <code>base.yml</code>에 정의되어 있다고 가정한다.

```
theme:
    name: mkdocs
    locale: en
    highlightjs: true

markdown_extensions:
    toc:
        permalink: true
    admonition: {}
```

그런 다음 "foo" 사이트의 경우 주 설정 파일 <code>foo/mkdocs.yml</code>은 아래와 같이 정의할 수 있다.

```
INHERIT: ../base.yml
site_name: Foo Project
site_url: https://example.com/foo
```

mkdocs 빌드를 실행할 때 <code>foo/mkdocs.yml</code> 파일이 설정 파일로 전달됩니다. 그런 다음 MkDocs는 해당 파일을 파싱하고 상위 파일 <code>base.yml</code>을 찾아 파싱한 다음 이 두 파일을 병합한다. 이렇게 하여 MkDocs가 다음과 같은 병합된 설정을 얻게 된다.

```
site_name: Foo Project
site_url: https://example.com/foo

theme:
    name: mkdocs
    locale: en
    highlightjs: true

markdown_extensions:
    toc:
        permalink: true
    admonition: {}
```

병합을 사용하면 주 설정 파일에 다양한 값을 추가하거나 오버라이드할 수 있습니다. 예를 들어, 정의 목록에 대한 지원을 추가하고, 퍼마링크에 다른 기호를 사용하고, 다른 구분 문자를 정의하려고 하는 사이트를 가정하면, 해당 사이트의 주 설정 파일에 다음을 수행할 수 있다.

```
INHERIT: ../base.yml
site_name: Bar Project
site_url: https://example.com/bar

markdown_extensions:
    def_list: {}
    toc:
        permalink: 
        separator: "_"
```

이 경우 위의 설정이 <code>base.yml</code>과 병합되어 다음과 같은 설정이 된다.
```
site_name: Bar Project
site_url: https://example.com/bar

theme:
    name: mkdocs
    locale: en
    highlightjs: true

markdown_extensions:
    def_list: {}
    toc:
        permalink: 
        separator: "_"
    admonition: {}
```

부모 설정의 <code>admonition</code> 확장이 유지되고, <code>def_list</code> 확장이 추가되었으며, <code>toc.permalink</code> 값이 대체되었으며, <code>toc.separator</code> 값이 추가되었다.

키의 값을 바꾸거나 병합할 수 있다. 그러나 키가 아닌 값은 항상 교체된다. 따라서 리스트 항목에 추가할 수 없고 전체 리스트를 다시 정의해야 한다.

[nav](#nav) 설정은 중첩된 리스트로 구성되어 있으므로 탐색 항목을 병합할 수 없다. 따라서 전체 내비게이션 설정을 새 설정으로 바꾸어야 한다. 그러나 일반적으로 전체 탐색은 프로젝트의 주 구성 파일에서 정의할 것이로 예상되므로 크게 신경 쓸 필요가 없다.

> Warning <br>
> 모든 경로 기반 설정 옵션은 주 구성 파일에 상대적이어야 하고 MkDocs는 병합할 때 경로를 변경하지 않는다. 따라서 서로 다른 여러 사이트에 상속되는 상위 파일의 경로를 정의하는 작업이 예상대로 작동하지 않을 수 있다. 일반적으로 주 설정 파일에서만 경로 기반 옵션을 정의하는 것이 바람직하다.
