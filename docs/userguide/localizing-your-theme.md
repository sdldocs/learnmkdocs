# 테마 현지화
> Note <br>
> 테마 현지화는 문서의 실제 내용이 아닌 테마 자체의 텍스트 요소(예: "next" 및 "previous" 링크)만 번역한다. 다국어 문서를 작성하려면 이곳의 설명처럼 테마 현지화를 서드 파티 국제화/현지화 플러그인과 결합해야 한다.

## 설치 <a id="installation"></a>
테마 현지화를 작동하려면 이를 지원하는 테마를 사용하고 <code>mkdocs[i18n]</code>를 설치하여 <code>i18n</code>(국제화) 지원을 활성화해야 한다.

```python
pip install mkdocs[i18n]
```

## 지원하는 로케일 <a id="supported-locale"></a>
대부분의 경우 로케일은 해당 언어의 [ISO-639-1](https://en.wikipedia.org/wiki/ISO_639-1)(2글자) 약어로 지정되어 있다. 그러나 로케일에 지역 코드가 포함될 수 있다. 언어와 영역은 밑줄로 구분된다. 예를 들어, 영어에 사용 가능한 로케일로는 <code>en</code>, <code>en_AU</code>, <code>en_GB</code> 및 <code>en_US</code>가 포함될 수 있다.

사용 중인 테마에서 지원되는 로케일 목록을 보려면 해당 테마 문서를 참조하여야 한다.

- [mkdocs](../choosing-your-theme/#mkdocs-locale)
- [readthedocs](../choosing-your-theme/#readthedocs-locale)

> Warning <br>
> 사용 중인 테마에서 아직 지원되지 않는 언어 로케일을 설정하면 MkDocs는 테마의 기본 로케일로 돌아간다.

## 사용 <a id="usage"></a>
MkDocs에서 사용할 로케일을 지정하려면 [테마](../configuration/#theme) 설정 옵션의 [로케일](../configuration/#locale) 변수를 적절한 코드로 설정하여야 한다.

예를 들어, 프랑스어로 <code>mkdocs</code> 테마를 작성하려면 다음과 같이 <code>mkdocs.yml</code> 설정 파일을 작성하여야 한다.

```yml
theme:
     name: mkdocs
     locale: fr
```

## 테마 번역에 기여 <a id="contributing-theme-translation"></a>
테마가 아직 해당 언어로 번역되지 않은 경우, [Translation Guide](https://www.mkdocs.org/dev-guide/translations/)를 사용하여 번역을 제공할 수 있다. (**To do for kr**)
