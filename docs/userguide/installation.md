# MKDocs 설치

## Requirements
MkDocs 설치를 위하여 [Python](https://www.python.org/) 최신 버전과 Python package manager [pip](https://pip.readthedocs.io/en/stable/installing/)이 설치되어 있어야 한다. 

다음 명령어로 시스템에 설치되어 있는 위의 Python과 pip의 버전을 알 수 있다.
```
$ python3 --version
Python 3.8.2
$ pip --version
pip 20.0.2 from /usr/local/lib/python3.8/site-packages/pip (python 3.8)
```

위의 두 패키지 설치되어 있다면, 건너 뛰어 [MkDocs-설치하기](#installing-mkdocs)로 이동한다.

### Python 설치
(To be filled)

###  pip 설치
(To be filled)

## MkDocs 설치하기
pip 명령으로 <code>mkdocs</code> 패키지를 설치할 수 있다.
```
pip install mkdocs
```
<code>mkdocs</code> 설치가 성공적이면, <code>mkdocs --version</code> 명령을 수행하여 확인할 수 있다.
```
$ mkdocs --version
mkdocs, version 1.2.0 from /usr/local/lib/python3.8/site-packages/mkdocs (Python 3.8)
```

> Note: <br>
> 설치된 MkDocs에 대한 manpages 지원을 원하면, 아래 간단한 두 명령으로 [click-man](https://github.com/click-contrib/click-man)을 사용하여 생성하며 설치할 수 있다. 
> ```
> pip install click-man
> click-man --target path/to/man/pages mkdocs
> ```
> pip으로 manpage가 자동으로 생성되어 설치할 수 없는 이유에 대하여  [click-man 문서](https://github.com/click-contrib/click-man#automatic-man-page-installation-with-setuptools-and-pip)에 설명되어 있다.

> Note: <br>
> Windows에서는 위의 명령들이 작동하지 않을 수 있다. 빠른 해결 방법으로는 아래와 같이 Python 명령 앞에 <code>python -m</code>를 붙여 사용하는 것이다. <br>
>
> ```
> python -m pip install mkdocs 
> <br>
> python -m mkdocs
> ```
>
> 보다 영구적인 방법으로 Python을 설치한 <code>Scripts</code> 디렉터리를 포함하도록 <code>PATH</code> 환경 변수를 편집해야 할 수도 있다. Python 최신 버전에는 이 작업을 수행하는 스크립트가 포함되어 있다. Python 설치 디렉토리(예를 들어, C;\Python38\)에서 <code>Tools</code>, 그리고 <code>Scripts</code> 폴더를 열어 <code>win_add2path.py</code>를 더블 클릭하여 실행한다. 다른 방법으로는 [script](https://github.com/python/cpython/blob/master/Tools/scripts/win_add2path.py)를 다운로드 받아 이를 실행 (<code>python win_add2_path.py</code>)한다.
