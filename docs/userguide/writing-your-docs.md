# 문서 작성

Markdwon 소스 파일의 레이아웃과 작성

## 파일 레이아웃
일반 마크다운 파일 형식으로 문서 원본을 작성하여야 하며(아래 [Markdown으로 문서 작성](#writing-with-markdown) 참조), [문서 디렉토리](configuration/#docs_dir)에 저장되어야 한다. 기본적으로 이 디렉터리 <code>docs</code>는 설정 파일 <code>mkdocs.yml</code>과 함께 프로젝트의 최상위 폴더에 저장되어야 한다.

가장 긴단한 프로젝트는 아래와 같다.
```
mkdocs.yml
docs/
    index.md
```

일반적으로 프로젝트 홈페이지는 <code>index.md</code>라는 이름의 파일이어야 한다 (자세한 내용은 아래 [Index pages](#index-page) 참조). 마크다운 소스 파일은 <code>mkardown</code>, <code>mkdn</code>, <code>mkd</code> 또는 <code>md</code>를 파일 확장자로 갖는다. 문서 디렉토리에 저장된 모든 Markdown 파일은 설정에 관계없이 빌드 사이트에 렌더링된다.

> Note: <br>
> 대부분의 웹 서버를 실행할 때 Mkdocs는 점('<code>.</code>')으로 시작하는 파일 및 디렉터리 이름(예: <code>.foo.md</code> 또는 <code>.bar/baz.md</code>)를 인식하지 못한다. 더우기 이를 재정의할 수 있는 방법이 없다. 이를 유념하여야 한다.

다수의 Markdown 파일을 생성하여 여러 패이지의 문서를 만들 수 있다.
```
docs/
    index.md
    about.md
    license.md
```

정의한 파일 레이아웃이 생성된 페이지에 사용되는 URL를 결정한다.
```
/
/about/
/license/
```

작성하는 문서 레이아웃에 바람직하다면 다음과 같이 디렉토리 내에 Markdown 파일을 포함할 수 있다.
```
docs/
    index.md
    userguide/hetting-started.md
    userguide/configuration-options.md
    license.md
```

디렉토리에 저장된 소스 파일은 다음과 같이 생성된 URL 내의 페이지로 출력된다.
```
/
/userguide/getting-started/
/userguide/configuration-options/
/license/
```

[docs 디렉토리]9https:userguide/configuration/#docs_dir) 내의 Markdown 파일이 아닌 다른 파일(파일 확장자가 Markdwon 파일 확장자와 다른 파일)들을 MkDocs는 빌드 사이트로 복사한다. [이미지 및 미디어에 연결하는 방법](/userguide/writing-your-docs/#linking-to-images-and-media)에서 보다 자세히 설명한다.

### Index pages <a id="index-pages"></a>
디렉터리가 요청되면 대부분의 웹 서버는 기본적으로 해당 디렉터리내의 인덱스 파일(일반적으로 <code>index.html</code>)을 반환한다. 그리하여 위의 모든 예에 있는 홈페이지에는 index.md가 있으며, MkDocs는 사이트를 구축할 때 <code>index.html</code>으로 렌더링한다.

많은 리포지토리 호스팅 사이트들은 디렉토리의 내용을 탐색할 때 README 파일의 내용을 디스플레이함으로써 README 파일을 특별하게 취급한다. 따라서 MkDocs를 사용할 때 인덱스 페이지의 이름을 <code>index.md</code> 대신 <code>README.md</code>으로 지정할 수 있다. 이 방식으로 사용자가 소스 코드를 검색할 때 리포지토리 호스트는 README 파일을 디렉토리의 인덱스 페이지로 디스플레이할 수 있다. 그러나 MkDocs가 사이트를 렌더링할 때 서버는 적합한 색인 파일로 사용할 수 있도록 파일 이름을 <code>index.html</code>로 변경한다.

만약 <code>index.md</code>와 <code>README.md</code> 모두 디렉토리내에 인다면, <code>index.md</code이 사용되고 code>README.md</code>는 무시된다.

### 페이지와 탐색 설정 <a id="configure-pages-and-navigation"></a>
<code>mkdocs.yml</code> 파일의 [nav](/userguide/configuration/#nav) 설정은 전역 사이트 탐색 메뉴에 포함되는 페이지와 해당 메뉴의 구조를 정의한다. 이를 정의하지 않는 경우 [문서 디렉토리](/userguide/configuration/#docs_dir) 안의 모든 마크다운 파일을 검색하여 네비게이션을 자동으로 생성한다. (인덱스 파일이 항상 하위 섹션에서 먼저 나타나는 경우를 제외하고) 자동으로 작성되는 네이비게이션은 항상 파일 이름의 알파벳 순으로 정렬하여 구성된다. 탐색 메뉴를 다르게 정렬하려면 탐색 구성을 상요자가 직접 정의해야 한다.

최소 탐색 설정은 다음과 같다.
```
nav:
    - 'idex.md'
    - 'about.md'
```
탐색 설정의 모든 경로는 <code>docs_dir</code> 설정 옵션에 상대적이어야 한다. 이 옵션이 기본값인 <code>docs</code>로 설정되어 있다면 위의 설정에 대한 소스 파일은 각각 <code>docs/index.md</code>과 <code>docs/about.md</code>이다.

위의 예에서 Markdown 파일의 내용이나 파일 이름에 제목이 없는 경우 상위 레벨에서 두 개의 탐색 항목이 생성될 수 있다. 탐색 설정에서 제목을 달려면 다음과 같이 파일 이름 바로 앞에 제목을 추가할 수 있다.
```
nav:
    - Home: 'idex.md'
    - About: 'about.md'
```

네비게이션에 페이지에 대한 제목을 설정하였다면, 해당 제목은 해당 페이지에 대한 사이트 전체에서 사용되며 페이지 자체에 정의된 제목보다 우선한다.

섹션 제목 하에 관련 페이지를 나열하여 네비게이션 하위 섹션을 작성할 수 있습니다. 예:
```
nav:
    - Home: 'index.md'
    - 'User Guide':
        - 'Writing your docs': 'writing-your-docs.md'
        - 'Styling your docs': 'styling-your-docs.md'
    - About:
        - 'License': 'lecense.md'
        - 'Release Notes': 'release-notes.md'
```

위의 설정 예에는 "Home", "User Guide" 및 "About"의 세 최상위 항목이 있다. "Home"은 사이트 홈페이지 링크이다. "User Guide" 섹션에는 "Writing your docs"와 "Styling your docs" 두 페이지가 있다. "About" 섹션에는 "License"와 "Release Notes" 두 페이지가 더 있다.

섹션에는 페이지를 할당할 수 없다. 섹션은 하위 페이지 및 하위 섹션의 컨테이너 역할만을 한다. 원하는 깊이 만큼 하위 섹션을 만들 수 있다. 그러나 구조를 지나치게 복잡하게 구성하여 사용자가 사이트 탐색을 어렵게 만들지 않도록 주의하여야 한다. 섹션은 디렉토리 구조를 반영할 수 있지만 꼭 그럴 필요는 없다.

탐색 설정에 나타나지 않은 페이지는 여전히 렌더링되고 빌드 사이트에 포함되지만 다른 페이지들에서 링크되지 않으며 <code>previous</code>와 <code>next</code> 링크에도 포함되지 않는다. 이러한 페이지는 직접 연결되지 않는 한 "숨겨진 페이지"로 취급된다.

## Markdown 문서 작성 <a id="writing-with-markdown"></a>
읽기 쉽고 쓰기 쉬운 일반 텍스트 문서를 HTML 문서로 변환할 수 있는 간편한 마크업 언어인 [Markdown](https://daringfireball.net/projects/markdown/)으로 MkDocs 페이지를 작성하어야 한다.

Markdown 문서를 HTML로 렌더링하기 위하여 MkDocs는 [Python-Markdown](https://python-markdown.github.io/) 라이브러리를 사용한다. Python-Markdown은 극히 사소한 묯가지 [차이](https://python-markdown.github.io/#differences)가 있지만 [레퍼런스 구현](https://daringfireball.net/projects/markdown/)을 거의 완전히 준수하고 있다.

MkDocs는 모든 마크다운 구현에 공통으로 적용되는 기본 마크다운 [구문](https://daringfireball.net/projects/markdown/syntax) 외에도 Python-Markdown [확장](https://python-markdown.github.io/extensions/)을 통해 마크다운 구문을 확장할 수 있도록 지원하고 있다. 확장을 활성화하는 방법에 대한 내용을 MkDocs의 [markdown_extensions](/userguide/configuration/#markdown_extensions) 설정에서 자세히 설명하고 있다.

MkDocs에는 기본적으로 아래에서 설명하는 일부 확장이 포함되어 있다.

### 내부 링크
MkDocs를 사용하면 일반 마크다운 [링크](https://daringfireball.net/projects/markdown/syntax#link)를 사용하여 문서를 연결할 수 있다. 그러나 아래에서 기술하는 MkDocs 전용 링크를 이용하면 몇 가지 추가적인 이점을 얻을 수 있다.

#### 페이지로 링크
문서에서 페이지 간을 연결할 때 연결할 마크다운 문서의 *상대 경로*를 포함하여 일반적인 마크다운 [연결](https://daringfireball.net/projects/markdown/syntax#link) 구문을 사용한다.

```
Please see the [project license](license.md) for further details.
```

MkDocs 빌드가 실행될 때 이러한 마크다운 링크는 자동으로 HTML 하이퍼링크로 변환되어 해당 HTML 페이지로 이동을 지원한다.

> Warning <br>
> 링크에 절대 경로 사용을 공식적으로 지원하지 않고 있다. MkDocs는 상대 경로가 항상 페이지에 상대적이 되도록 조정한다. 절대 경로는 수정되지 않는다. 즉, 절대 경로를 사용하는 링크는 로컬 환경에서는 정상적으로 작동하지만 프로덕션 서버로 배포하면 연결되지 않을 수 있다.

대상 문서 파일이 다른 디렉터리에 있는 경우 링크에 상대 디렉터리 경로를 포함해야 한다.

```
Please see the [project license](../about/license.md) for further details.
```

앵커 ID는 헤더의 텍스트로부터 생성된다. 모든 헤더 텍스트를 소문자로 변환하고 공백과 같은 허용되지 않는 문자는 대시로 변환한다. 그런 다음 연속되는 대시는 단일 대시로 줄인다.

toc 확장자에 의해 지원되는 몇 가지 설정은 <code>mkdocs.yml</code> 설정 파일에서 기본 동작을 변경하도록 설정할 수 있다.

**<code>permalink</code>:**<br>
: 각 헤더의 끝에 영구 링크(permanent link)를 생성한다. 기본값: <code>False</code>.

: 참으로 설정되면 문단 기호(<code>&para;</code> 또는 <code>\&para;</code>)가 링크 텍스트로 사용됩니다. 문자열로 설정하면 제공된 문자열이 링크 텍스트로 사용됩니다. 예를 들어 해시 기호(<code>#</code>)를 대신 사용하려면 다음과 같이 할 수 있다.

```
markdown_extensions:
    - toc:
        permalink: "#"
```

**<code>baselevel</code>:**<br>
: 헤더의 base 레벨. 기본값: <code>1</code>.

: 이 설정을 사용하면 HTML 템플리트 계층에 맞게 헤더 단계를 자동으로 조정할 수 있다. 예를 들어, 페이지의 Markdown 텍스트에 2 단계(<<code>h2</code>>)보다 높은 단계의 헤더가 포함되지 않아야 하는 경우, 다음과 같이 설정할 수 있다.

```
markdown_extensions:
    - toc:
        baselevel: 2
```

: 그러면 문서의 헤더는 1씩 증가합니다. 예를 들어, 헤더 # Header는 HTML 출력에서 레벨 2 헤더(<code>\<h2\></code>)로 렌더링된다.

**<code>seperator</code>:**<br>
: 단어 분리 문자. 기본값: <code>-</code>.

: 생성되는 ID의 공백을 대체하는 문자이다. 밑줄을 선호하는 경우 다음과 같이 설정할 수 있다.

```
markdown_extensions:
    - toc:
        seperator: "-"
```

위의 설정을 모두 함꼐 정의하려면 markdown_extensions 설정에서 단일 toc 항목에서 정의하면 된다.

```
markdown_extensions:
    - toc:
        permalink: "#"
        baselevel: 2
        separator: "_"
```

#### 이미지 및 미디어로 링크하기 <a id="linking-to-images-and-media"></a>
마크다운 소스 파일뿐만 아니라 문서 사이트를 생성할 때 이미지 및 기타 미디어 파일과 같이 복사되는 다른 파일 형식도 문서에 포함할 수 있다.

예를 들어 프로젝트 문서에 [GitHub pages CNAME 파일](https://help.github.com/articles/using-a-custom-domain-with-github-pages/)과 PNG 형식의 스크린샷 이미지를 포함해야 하는 경우 파일 레이아웃은 다음과 같다.

```
mkdocs.yml
docs/
    CNAME
    index.md
    about.md
    license.md
    img/
        screenshot.png
```

이미지를 문서 소스 파일에 포함시키려면 일반적인 마크다운 이미지 구문을 사용하면 된다.

```
Cupcake indexer is a snazzy new project for indexing small cakes.

![Screenshot](img/screenshot.png)

*Above: Cupcake indexer in progress*
```

이제 문서를 빌드할 때 이미지가 포함되며 Markdown 편집기로 문서를 작업하는 경우 미리 볼 수 있어야 한다.

#### HTML에서 링크
Markdown에서는 Markdown 문법이 문서 작성자의 요구를 충족하지 못할 때 HTML을 사용할 수 있도록 한다. MkDocs는 이와 관련하여 Markdown에 제한두지 않는다. 그러나 HTML이 Markdown 파서에 의해 무시되므로, MkDocs는 HTML에 포함된 링크를 확인하거나 변환할 수 없다. 따라서 HTML에 내부 링크가 있다면, 렌더링된 문서에 적합한 링크 형식을 수동으로 지정해야 한다.

### 메타 데이터 <a id="meta-data"></a>
MkDocs는 YAML과 MultiMarkdown 스타일 메타 데이터(종종 프론트 매터라고도 함)를 지원한다. 메타 데이터는 Markdwon 문서의 시작 부분에 정의된 일련의 키워드와 그 값으로 구성되며, Python-Markdown는 처리하기 전에 문서에서 이를 제거된다. MkDocs은 키/값 쌍을 페이지 템플릿에게 전달한다. 따라서 테마 지원이 필요한 경우, 임의의 키 값을 페이지에  출력하거나 페이지 렌더링을 제어하는 데 사용할 수 있다. 지원되는 키가 있는 경우 테마 문서에서 그 정보에 대하여 설명한다.

템플릿에 정보를 디스플레이하는 것 외에도 MkDocs는 특정 페이지에 대한 MkDocs의 작동을 변경할 수 있는 정의된 메타 데이터 키를 지원한다. 지원되는 키는 다음과 같다.

**<code>template</code>:**<br>

: 현재 페이지에서 사용하는 템플릿.

: 기본적으로 MkDocs는 테마의 <code>main.html</code> 템플릿을 사용하여 Markdown 페이지를 렌더링한다. <code>template</code> 메타 데이터 키를 사용하여 특정 페이지에 대해 다른 템플릿 파일을 정의할 수 있다. 템플릿 파일은 테마 환경에 정의된 경로에서 사용할 수 있어야 한다.

**<code>title</code>:**<br>

: 문서에서 사용하는 "제목".

: MkDocs는 다음과 같은 방법으로 문서 제목을 결정한다.

:  1. 문서의 [nav](/userguide/configuration/#nav) 설정에서 정의된 제묵.
   2. 문서의 <code>title</code> 메타 데이터 키에서 정의한 제목.
   3. 문서 바디의 첫 줄에 있는 레벨 1 Markdown 헤더. [Setext-style](https://daringfireball.net/projects/markdown/syntax#header) 헤더를 지원하지 않음에 주의해야 한다.
   4. 문서를 저장한 파일명.

: 페이지 제목을 찾으면 MkDocs는 위의 목록에서 추가적인 소스 확인 작업를 더이상 하지 않는다.

#### YAML 스타일 메타데이터

YAML 스타일 메타 데이터는 YAML 스타일 딜리머네이터로 래핑된 [YAML](https://www.mkdocs.org/userguide/writing-your-docs/#:~:text=is%20parsed%20as-,YAML,-.) 키/값 쌍으로 구성되어 메타데이터의 시작 및/또는 끝을 표시한다. 문서의 첫 번째 줄은 ---이어야 한다. 메타데이터는 끝 딜리머네이터(--- 또는 ...)를 포함하는 첫 줄에서 끝난다. 딜리머네이터 간의 내용은 [YAML](https://www.mkdocs.org/user-guide/writing-your-docs/#:~:text=is%20parsed%20as-,YAML,-.)으로 파싱된다.
```
---
title: My Document
summary: A brief description of my document.
authors:
    - Waylan Limberg
    - Tom Christie
date: 2018-07-10
some_url: https://example.com
---
This is the first paragraph of the document.
```

YAML은 데이터 타입을 탐지할 수 있다. 따라서 위의 예에서 <code>title</code>, <code>summary</code>과 <code>some_url</code>의 값은 문자열이고, <code>author</code>의 값은 문자열 리스트이며, <code>date</code> 값은 <code>datetime.date</code> 객체이다. YAML 키는 대소문자를 구분하며 MkDocs는 키는 모두 소문자라고 여긴다. YAML의 최상위 수준은 Python <code>dict</code>를 반환하는 키/값 쌍의 콜렉션이어야 한다. 다른 유형이 반환되거나 YAML 파서에서 오류가 발생하는 경우, MkDocs는 섹션을 메타 데이터로 인식하지 못하며 페이지의 <code>meta</code> 속성은 비어 있으며 섹션을 문서에서 제거하지 않는다.

#### MultiMarkdown 스타일 메타데이터
MultiMarkdown 스타일 메타데이터는 [MultiMarkdown](http://fletcherpenney.net/MultiMarkdown_Syntax_Guide#metadata) 프로젝트에서 처음 도입된 형식을 사용한다. 데이터는 다음과 같이 Markdown 문서의 시작 부분에 정의된 일련의 키워드와 값으로 구성된다.

```
Title:   My Document
Summary: A brief description of my document.
Authors: Waylan Limberg
         Tom Christie
Date:    January 23, 2018
blank-value:
some_url: https://example.com

This is the first paragraph of the document.
```

키워드는 대소문자를 구분하지 않으며 문자, 숫자, 밑줄 및 대시로 구성될 수 있으며 콜론으로 끝난다. 값은 콜론 뒤에 그 줄에 나타난 값으로 구성되며 공백일 수도 있다.

행이 4개 이상의 공백으로 들여쓰기된 경우 해당 행은 이전 키워드에 대한 값의 추가 행으로 간주된다. 키워드는 원하는 만큼의 행을 가질 수 있다. 모든 행을 하나의 문자열로 결합한다.

첫번째 빈 줄까지 문서의 메타데이터이다. 따라서 문서의 첫 번째 줄을 비워두면 안된다.

> **Note**<br>
> MkDocs는 MultiMarkdown 스타일 메타데이터에 대해 YAML 스타일 딜리머네이터(<code>---</code> 또는 <code>...</code>)를 지원하지 않는다. 실제로 MkDocs는 YAML 스타일 메타 데이터 또는 MultiMarkdown 스타일 메타 데이터가 사용되고 있는지 여부를 결정하기 위해 딜리머네이터 유무에 의존한다. 딜리머네이터가 탐지되었지만 딜리머네이터 간의 내용이 유효한 YAML 메타데이터가 아닌 경우 MkDocs는 내용을 MultiMarkdown 스타일 메타 데이터로 파싱하려고 시도하지 않는다.


### 테이블
[Table](https://python-markdown.github.io/extensions/tables/) 확장은 여러 구현에서 널리 사용되는 Makkdown의 기본 테이블 구문을 추가한다. 이 문법은 다소 단순하며 일반적으로 단순한 표 형식 데이터에만 유용하다.

단순한 테이블은 아래와 같다.
```
First Header | Second Header | Third Header
------------ | ------------- | ------------
Content Cell | Content Cell  | Content Cell
Content Cell | Content Cell  | Content Cell
```

원하면 테이블의 각 행에 시작과 끝 pipe를 추가할 수 있다.
```
| First Header | Second Header | Third Header |
| ------------ | ------------- | ------------ |
| Content Cell | Content Cell  | Content Cell |
| Content Cell | Content Cell  | Content Cell |
```

각 열에 구분선을 추가하여 얼라인먼트를 지정할 수 있다.
```
First Header | Second Header | Third Header
:----------- |:-------------:| -----------:
Left         | Center        | Right
Left         | Center        | Right
```

표의 셀에 블록 레벨 요소가 올 수 없으며 여러 줄의 문자열도 올 수 없다. 그러나 이들은 Markdown [구문](https://daringfireball.net/projects/markdown/syntax) 규칙에 정의된 인라인 Markdown을 포함할 수 있다.

또한 테이블은 빈 줄로 둘러 싸야 있어야 한다. 즉 테이블 앞과 뒤에 빈 줄이 있어야 한다.

### 코드 블럭
[Fenced code block](https://python-markdown.github.io/extensions/fenced_code_blocks/) 확장은 들여쓰기 없이 코드 블록을 정의하는 다른 방법을 추가한다.

첫 줄에 3자 이상의 백티크(<code>\`</code>) 문자가 나타나야 하며 마지막 줄에도 동일한 수의 백티크 문자(<code>\`</code>)가 있어야 한다.


\`\`\`<br>
Fenced code blocks are like Standard
Markdown’s regular code blocks, except that
they’re not indented and instead rely on
start and end fence lines to delimit the
code block.<br>
\`\`\`

이 방식으로 백틱 뒤 첫 번째 줄에 사용하는 언어를 선택적으로 지정할 수 있다.

\`\`\`python<br>
def fn():<br>
    pass<br>
\`\`\`

코드 블록은 들여쓸 수 없다. 따라서 리스트 항목, 블록 따옴표 등은 블럭 내부에 중첩될 수 없다.

