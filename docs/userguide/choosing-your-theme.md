# 테마 선택 <a id="choosing-your-theme"></a>
테마의 선택과 설정

설명하겠지만 MkDocs는 2개의 테마([mkdocs](#mkdocs)와 [readthedocs](#readthedocs))를 내장하고 있다.

설정 파일 <code>mkdocs.yml</code>의 [테마](/userguide/configuration/#theme) 설정 옵션을 설정하여 테마를 선택한다.

```
theme:
    name: readthedocs
```

## mkdocs <a id="mkdocs"></a>
사용자 지정 [Bootstrap](https://getbootstrap.com/) 테마로 구축된 기본 테마는 MkDocs의 대부분의 기능을 지원한다.

![default](../images/fig-20.png)

기본 [테마 설정 옵션](/userguide//configuration/#theme) 외에도 mkdocs 테마는 다음 옵션을 지원한다.

- **<code>highlightjs</code>**: JavaScript 라이브러리 [highlight.js](https://highlightjs.org/)를 사용하여 코드 블록에서 소스 코드를 강조하여 디스플레이한다. 기본값: <code>True</code>
- **<code>hljs_style</code>**: highlight.js 라이브러리는 코드 블록에서 소스 코드를 강조 표시할 수 있는 79개의 다른 [스타일](https://highlightjs.org/static/demo/)(색상 변형)을 제공하고 있다. 원하는 스타일의 이름으로 설정할 수 있다. 기본값: <code>github</code>.
- **<code>hljs_languages</code>**: 기본적으로 highligh.js눈 공용 언어 23개를 지원하고 있다. 여기에 추가적으로 필요한 언어를 나열하여야 한다.

```
theme:
    name: mkdocs
    highlight.js: true
    hljs_languages:
        - yaml
        - rust
```

- **<code>analytics</code>**: 분석 서비스의 설정 옵션을 정의할 수 있다. 현재는 <code>gtag</code> 옵션을 통해 Google Analytics v4만 지원하고 있다.
    - **<code>gtag</code>**: Google Analytics를 사용하려면 <code>G-</code> 형식을 사용하는 Google Analytics v4 추적 ID로 설정하여야 한다. Google 설명서를 참고하여 [웹 사이트 및/또는 앱(GA4)에 대한 분석을 설정](https://support.google.com/analytics/answer/9304153?hl=en&ref_topic=9303319)하거나 [Google Analytics 4로 업그레이드](https://support.google.com/analytics/answer/9744165?hl=en&ref_topic=9303319) 할 수 있다. 기본값(null)으로 설정하면 사이트에 대해 Google Analytics가 비활성화된다.

```
    theme:
        name: mkdocs<br>
        analytics:
            - gtag: G-ABC123
```

- **<code>shortcuts</code>**: 키보드 숏컷 키를 정의한다.
    - **<code>help</code>**: 키보드 숏컷을 리스트하여 키보드 사용을 돕니다. 기본값: <code>191</code> (?)
    - **<code>next</code>**: "다음" 페이지로 이동한다. 기본값: <code>78</code> (n)
    - **<code>previous</code>**: "이전" 페이지로 이동한다. 기본값: <code>80</code> (p)
    - **<code>search</code>**: 검색 방법을 디스플레이한다. 기본값: <code>83</code> (s)
```
theme:
    name: mkdocs
    shortcuts:
        help: 191   # 7
        next:   78  # n
        previous: 80    # p
        search: 83  # s
```

모든 값은 숫자 키 코드여야 한다. 모든 키보드에서 사용할 수 있는 키를 사용하는 것이 바람직 하다. [https://keycode.info/](https://keycode.info/)에서 지정된 키의 키 코드를 확인할 수 있다.

- **<code>navigation_depth</code>**: 사이드 바에 있는 탐색 트리의 최대 깊이. 기본값: <code>2</code>
- **<code>nav_style</code>**: 이는 상단 탐색 바의 시각적 스타일을 조정하며, 기본값 <code>primary</code>로 설정되지만 어둡거나 밝게 설정할 수도 있다.
    ```
    theme:
        name: mkdocs
        nav_style: dark
    ```
- **<code>locale</code>**<a id="mkdocs-locale"></a>: 테마를 작성하는 데 사용되는 로케일(언어/위치)이다. 지원하지 않는 로케일은 기본값으로 폴백된다. 이 테마에서는 다음 로케일을 지원한다.
    - <code>en</code>: English (기본값)
    - <code>fr</code>: French
    - <code>es</code>: Spanish
    - <code>ja</code>: Japanese
    - <code>pt_BR</code>: Portuguese (Brazil)
    - <code>zh_CN</code>: Simplified Chinese

    [테마 현지화](/userguide/localizing-your-theme/)에서 보다 자세한 정보를 찾을 수 있다.

## readthedocs<a id="readthedocs"></a>
[Read the Docs](https://readthedocs.org/) 서비스에서 사용하는 기본 테마의 클론으로, 상위 테마와 동일한 제한된 기능 집합을 제공한다. 상위 테마와 마찬가지로 두 가지 수준의 탐색만 지원한다.

![readthedocs-theme](../images/fig-21.png)

<code>readthedocs</code> 테마에서는 기본 [테마 설정 옵션](/userguide/configuration/#theme) 외에도 다음과 같은 옵션을 지원한다.

- **<code>highlightjs</code>**: JavaScript 라이브러리 [highlight.js](https://highlightjs.org/)를 사용하여 코드 블록에서 소스 코드를 강조하여 디스플레이한다. 기본값: <code>True</code>

- **<code>hljs_languages</code>**: 기본적으로 highligh.js눈 공용 언어 23개를 지원하고 있다. 여기에 추가적으로 필요한 언어를 나열하여야 한다.

```
theme:
    name: readthedocs
    highlight.js: true
    hljs_languages:
        - yaml
        - rust
```

- **<code>analytics</code>**: 분석 서비스의 설정 옵션을 정의할 수 있다. 현재는 <code>gtag</code> 옵션을 통해 Google Analytics v4만 지원하고 있다.
    - **<code>gtag</code>**: Google Analytics를 사용하려면 <code>G-</code> 형식을 사용하는 Google Analytics v4 추적 ID로 설정하여야 한다. Google 설명서를 참고하여 [웹 사이트 및/또는 앱(GA4)에 대한 분석을 설정](https://support.google.com/analytics/answer/9304153?hl=en&ref_topic=9303319)하거나 [Google Analytics 4로 업그레이드](https://support.google.com/analytics/answer/9744165?hl=en&ref_topic=9303319) 할 수 있다. 기본값(null)으로 설정하면 사이트에 대해 Google Analytics가 비활성화된다.

```
    theme:
        name: readthedocs
        analytics:
            - gtag: G-ABC123
```

- **<code>include_bomepage_in_sidebar</code>**: 사이드바 메뉴에 홈페이지를 나열한다. MkDocs는 홈페이지를 <code>nav</code> 설정 옵션에 나열해야 하므로 이 설정을 통해 홈페이지를 사이드바에서 포함하거나 제외할 수 있다. 사이트 이름/로고는 항상 홈페이지에 연결된다. 기본값: <code>True</code>

- **<code>prev_next_buttons_location</code>**: <code>bottom</code>, <code>top</code>, <code>both</code> 또는 <code>none</code>중 하나. 이에 따라 "Next" 및 "Previous" 버튼을 디스플레이한다. 기본값: <code>bottom</code>.

- **<code>navigation_depth</code>**: 사이드 바에 있는 탐색 트리의 최대 깊이. 기본값: <code>4</code>

- **<code>collapse_navigation</code>**: 현재 페이지의 사이드 바에는 페이지 섹션 헤더만 디스플레이한다. 기본값: <code>True</code>

- **<code>title_only</code>**: 모든 페이지에 대한 섹션 헤더를 제외하고 페이지 제목만 사이드 바에 디스플레이한다. 기본값: <code>False</code>

- **<code>sticky_navigation</code>**: 참이면 페이지를 스크롤할 때 사이드 바가 주 페이지 내용과 함께 스크롤된다. 기본값: <code>True</code>

- **<code>locale</code>**<a id="readthedocs-locale"></a>: 테마를 작성하는 데 사용되는 로케일(언어/위치)이다. 지원하지 않는 로케일은 기본값으로 폴백된다. 이 테마에서는 다음 로케일을 지원한다.<br>
    - <code>en</code>: English (기본값)
    - <code>fr</code>: French
    - <code>es</code>: Spanish
    - <code>ja</code>: Japanese
    - <code>pt_BR</code>: Portuguese (Brazil)
    - <code>zh_CN</code>: Simplified Chinese

[테마 현지화](/userguide/localizing-your-theme/)에서 보다 자세한 정보를 찾을 수 있다.

## 서드 파티 테마 <a id="third-party-theme"></a>
MkDocs [community wiki](https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes)에서 서그 파티 테마 목록을 확인할 수 있다. 직접 작성한 경우 목록에 추가해 주십시오.

**To do**: Mkdocs materials을 추가할 것
