# 문서 배포

## GitHub 페이지 <a id="github-pages"></a>
프로젝트의 소스 코드를 [GitHub](https://github.com/)에서 호스트하는 경우 [GitHub Pages](https://pages.github.com/)를 사용하여 프로젝트의 문서를 호스트할 수 있다. GitHub Pages 사이트에는 [프로젝트 페이지](https://help.github.com/articles/user-organization-and-project-pages/#project-pages-sites) 사이트와 [사용자 및 조직 페이지](https://help.github.com/articles/user-organization-and-project-pages/#user-and-organization-pages-sites) 사이트의 두 가지 기본 유형이 있다. 이들은 거의 동일하지만 몇 가지 중요한 차이점이 있어 배포할 때 다른 작업 흐름이 필요하다.

### 프로젝트 페이지 <a id="projecy-pages"></a>
프로젝트 페이지 사이트는 사이트 파일이 프로젝트 리포지토리 내의 분기(기본적으로 <code>gh-pages</code>)에 배포되므로 더 간단하다. 프로젝트의 원본 문서를 유지하는 git 리포지토리의 주 작업 분기(통상적으로 <code>master</code>)를 <code>checkout</code>한 후 다음 명령을 실행한다.

```
mkdocs gh-deploy
```

이게 끝이다! 백그라운드에서 MkDocs는 문서를 작성하고 [ghp-import](https://github.com/davisp/ghp-import) 도구를 사용하여 <code>gh-page</code> 분기에 커밋하고 <code>gh-page</code> 분기를 GitHub에 푸시한다.

<code>mkdocs gh-deploy --help</code>를 사용하여 <code>gh-deploy</code> 명령에 사용할 수 있는 전체 옵션 목록을 볼 수 있다.

빌드된 사이트가 GitHub로 푸시되기 전에는 해당 사이트를 검토할 수 없다. 따라서 <code>build</code> 또는 <code>serve</code> 명령을 사용하여 빌드된 파일을 로컬에서 검토하면 문서에 대한 변경사항을 미리 확인할 수 있다.

> Warning <br>
> <code>>gh-deploy</code> 명령을 사용하는 경우 페이지 저장소의 파일을 직접 편집하지 않아야 한다. 왜냐하면 명령을 실행 한 다음 작업이 손실되기 때문이다. 

### 조직과 사용자 페이지 <a id="organization-and-user-pages"></a>
사용자 및 조직 페이지 사이트는 특정 프로젝트에 연결되지 않으며 사이트 파일은 GitHub 계정 이름으로 명명된 전용 리포지토리의 <master> 분기에 배포된다. 따라서 로컬 시스템에 있는 두 저장소의 작업 복사본이 필요하다. 예를 들어 다음과 같은 파일 구조를 생각해 볼 수 있다.

```
my-project/
    mkdocs.yml
    docs/
orgname.github.io/
```

프로젝트를 업데이트하고 확인한 후에는 디렉터리를 <code>orgname.github.io</code> 리포지토리로 변경하고 <code>mkdocs gh-deploy</code> 명령을 호출해야 한다.

```
cd ../orgname.github.io/
mkdocs gh-deploy --config-file ../my-project/mkdocs.yml --remote-branch master
```

<code>mkdocs.yml</code> 설정 파일은 더 이상 현재 작업 디렉토리에 없으므로 명시적으로 가리켜야 한다. 또한 배포 스크립트가 <code>master</code> 분기에 커밋되도록 알려야 한다. 기본값을 [remote_branch](/userguide/configuration/#remote_branch) 설정으로 오버라이드할 수 있지만 배포 스크립트를 실행하기 전에 디렉토리를 변경하지 않으면 프로젝트의 <code>master</code> 분기에 커밋된다.

빌드된 사이트가 GitHub로 푸시되기 전에는 해당 사이트를 검토할 수 없다. 따라서 <code>build</code> 또는 <code>serve</code> 명령을 사용하여 빌드된 파일을 로컬에서 검토하면 문서에 대한 변경사항을 미리 확인할 수 있다.

> Warning <br>
> <code>>gh-deploy</code> 명령을 사용하는 경우 페이지 저장소의 파일을 직접 편집하지 않아야 한다. 왜냐하면 명령을 실행 한 다음 작업이 손실되기 때문이다. 

### 커스텀 도메인 <a id="custom-domains"></a>
GitHub Pages는 [사용자 지정 도메인(custom domain)](https://help.github.com/articles/adding-or-removing-a-custom-domain-for-your-github-pages-site)을 사이트에 사용할 수 있도록 지원한다. GitHub로 문서화된 단계 외에도 MkDocs가 사용자 지정 도메인과 함께 작동하도록 하려면 한 단계를 추가로 수행해야 한다. <code>CNAME</code> 파일을 [docs_dir](/userguide/configuration/#docs_dir)의 루트에 추가해야 한다. 파일에는 단일 베어 도메인 또는 하위 도메인이 한 줄에 포함되어야 한다 (예: MkDocs의 [CNAME 파일](https://github.com/mkdocs/mkdocs/blob/master/docs/CNAME) 참조). 파일을 수동으로 만들거나 GitHub의 웹 인터페이스를 사용하여 사용자 지정 도메인을 설정할 수 있다 (Settings / Custom Domain 아래). 만약 사용자가 웹 인터페이스를 사용한다면, GitHub는 사용자를 위한 <code>CNAME</code> 파일을 생성하고 그것을 사용자의 "페이지" 분기의 루트에 저장할 것이다. 다음에 배포할 때 파일이 제거되지 않도록 <code>docs_dir</code>에 파일을 복사해야 한다. 파일이 <code>docs_dir</code>에 명확이 포함되어 있다면 MkDocs는 사용자가 gh-deploy 명령을 실행할 때마다 해당 파일을 빌드 사이트에 포함시키고 "페이지" 분기에 푸시한다.

커스텀 도메인을 작동하는 데 문제가 있는 경우 GitHub의 커스텀 도메인 [Troubleshooting custom domains](https://help.github.com/articles/troubleshooting-custom-domains/)를 참조한다.

## Read the Docs <a id="read-the-docs"></a>
[Read the Docs](https://readthedocs.org/)에서는 무료 문서 호스팅을 제공하고 있다. Mercurial, Git, Subversion 및 Baza를 포함한 주 버전 관리 시스템을 사용하여 문서를 가져올 수 있다. Read the Docs는 MkDocs를 특별히 지원한다. 사이트의 [지침](https://docs.readthedocs.io/en/stable/intro/getting-started-with-mkdocs.html)에 따라 리포지토리의 파일을 올바르게 정렬한 다음, 계정을 만들고, 공용 호스트에 리포지토리를 지정한다. 설정이 이루어졌으면,  커밋이 이 공용 리포지토리에 푸시가 이루어 잘 때마다 문서는 갱신된다.

> Note <br>
>
> Read the Docs가 제공하는 모든 [기능](https://docs.readthedocs.io/en/latest/features.html)을 이용하려면 MkDocs와 함께 제공되는 [Read the Docs 테마](/userguide/choosing-your-theme/#readthedocs)를 사용해야 한다. Read the Docs 설명서에서 참조할 수 있는 다양한 테마는 스핑크스 고유의 테마이며 MkDocs에서는 작동하지 않는다.

## 다른 호스팅 제공자 <a id="other-providers"></a>
정적 파일을 제공할 수 있는 모든 호스팅 공급자는 MkDocs에서 생성된 문서를 제공하는 데 사용할 수 있다. 모든 호스팅 제공업체에 문서를 업로드하는 방법을 문서화하는 것은 불가능하지만 다음 가이드를 통해 일반적인 도움을 받을 수 있다.

사이트를 (<code>mkdocs build</code> 명령을 사용하여) 빌드할 때 <code>mkdocs.yaml</code> 설정 파일의 <code>site_dir</code> 설정 옵션(<code>"site"</code>)에 할당된 디렉토리에 모든 파일이 작성되어 있어야 한다.  
일반적으로 해당 디렉토리의 내용을 호스팅 공급자 서버의 루트 디렉토리에 복사하기만 하면 된다. 호스팅 공급자의 설정에 따라 GUI 또는 CLI로 <code>ftp</code>, <code>ssh</code> 또는 <code>scp</code> 실행하여 파일을 전송해야 할 수 있다.

예를 들어 다음과 같이 명령어를 실행해야 할 수도 있다.

```
mkdocs build
scp -r ./site user@host:/path/to/server/root
```

물론 호스팅 업체와 적절한 도메인 이름을 가진 <code>호스트</code>를 사용하여 사용의 username으로 <code>user</code>를 교체해야 할 수도 있다. 또한 호스트의 파일 시스템 구성과 일치하도록 <code>/path/to/server/root</code>를 조정해야 한다.

자세한 내용은 호스팅 설명서에서 "ftp" 또는 "업로드 사이트"를 검색하여 아를 참고한다.

## 로컬 파일 <a id="local-files"></a>
문서를 서버에 호스팅하는 대신 파일을 직접 배포하여 <code>file://</code> 스키마를 사용하여 브라우저에서 볼 수 있다.

모든 최신 브라우저의 보안 설정으로 인해 일부 기능은 동일하게 작동하지 않을 수 있으며 일부 기능은 전혀 작동하지 않을 수 있다. 사실, 몇 가지 설정을 매우 구체적인 방법으로 커스터마이징할 필요가 있을 것이다.

* [site_uel](/userguide/configuration/#site_url):<br>
site_url은 빈 문자열로 설정되어야 하며, 이는 MkDocs가 <code>file://</code> 스키마에서 작동하도록 사이트를 구축하도록 지시합니다.

```
site_url: ""
```

* [site_directory_url](/userguide/configuration/#use_directory_urls): <br>
<code>use_directory_urls</code>를 <code>false</code>로 설정한다. 그렇지 않으면 페이지 간 내부 연결이 제대로 작동하지 않는다.

```
use_directory_urls: false
```

* [search](/userguide/configuration/#search): <br>
검색 플러그인을 비활성화하거나 <code>file://</code> 스티마와 함께 작동하도록 특별한 서드 파티 검색 플러그인을 사용해야 한다. 모든 플러그인을 사용하지 않도록 설정하려면 <code>plugins</code> 설정을 빈 목록으로 설정한다. 다른 플러그인을 사용하도록 설정한 경우 <code>search</code>가 목록에 포함되지 않도록 해야 한다.

```
plugins: []
```

문서를 작성할 때 모든 내부 링크는 [문서화된 대로](/userguide/writing-your-docs/#internal-links) 상대 URL을 사용해야 합니다. 문서의 판독기는 서로 다른 장치를 사용하거나 파일이 해당 장치의 다른 위치에 있을 수도 있다.

문서를 오프라인에서 볼 필요가 있는 경우 어떤 테마를 선택할지도 주의해야 한다. 많은 테마에서는 라이브 인터넷 연결이 필요하며 다양한 지원 파일을 위해 CDN을 사용한다. 테마안에 모든 지원 파일이 직접 포함된 테마를 선택해야 한니다.

사이트를 (<code>mkdocs build</code> 명령을 사용하여) 빌드할 때 <code>mkdocs.yaml</code> 설정 파일의 <code>site_dir</code> 설정 옵션(<code>"site"</code>)에 할당된 디렉토리에 모든 파일이 작성되어 있어야 한다. 일반적으로, 디렉토리의 내용을 복사하여 독자에게 배포하기만 하면 된다. 또는 서드 파티 도구를 사용하여 HTML 파일을 다른 문서 형식으로 변환할 수도 있다.

## 404 페이지 <a id="404-pages"></a>
MkDocs가 문서를 생성할 때 [빌드 디렉토리](/userguide/configuration/#site_dir)에 404.html 파일을 포함한다. 이 파일은 [GitHub](/userguide/deploying-your-docs/#github-pages)에 배포할 때 자동으로 사용되지만 사용자 지정 도메인에서만 사용된다. 다른 웹 서버가 이 기능을 사용하도록 구성되어 있지만 이 기능을 사용할 수 없는 경우도 있다. 자세한 내용은 선택한 서버의 설명서를 참조하여야 한다.


