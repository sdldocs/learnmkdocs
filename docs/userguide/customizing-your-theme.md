# 테마 사용자 정의
기존 테마를 몇 가지 수정하려는 경우 처음부터 자신만의 테마를 만들 필요가 없다. 일부 CSS 또는 JavaScript만 사소한 수정이 필요한 경우 [docs_dir을 사용](#using-the-docs_dir)할 수 있다. 그러나 템플릿 오버라이딩를 포함하여 보다 복잡한 사용자 정의의 경우 [custom_dir 테마 설정](#using-the-theme-custom_dir)을 사용해야 한다.

## docs-dir 사용 <a id="using-the-docs_dir"></a>
[extra_css](../configuration/#extra_css) 및 [extra_javascript](../configuration/#extra_javascript) 설정 옵션을 사용하여 기존 테마를 수정하고 커스터마이징할 수 있다. 이러한 파일을 사용하려면 [문서 디렉토리](../configuration/#docs_dir)에 CSS 또는 JavaScript 파일을 포함하기만 하면 된다.

예를 들어 설명서의 머리글 색상을 변경하려면 <code>extra.css</code>라는 파일을 만들어 Markdown 문서와 같이 저장한다. 그 파일에 다음과 같은 CSS를 추가한다.

```css
h1 {
    color: red;
}
```

> Note <br>
> [ReadTheDocs](../deploying-your-docs/#readthedocs)를 사용하여 문서를 배포하는 경우. 설정 파일에 포함할 CSS 및 JavaScript 파일을 명시적으로 나열해야 한다. 이렇게 하려면 다음 사항을 mkdocs.yml에 추가하여야 한다.

> ```yaml
> extra_css: [extra.css]
> ```

이러한 변경사항은 mkdocs serve를 실행할 때 디스플레이된다. 이미 실행 중인 경우 CSS 변경사항이 자동으로 선택되고 문서가 업데이트되었음을 확인해야 한다.

> Note <br>
> 추가된 CSS 또는 JavaScript 파일은 생성된 HTML 문서인 페이지 내용과 함께 디렉토리에 추가됩니다. JavaScript 라이브러리를 포함하려면 [custom_dir](../configuration/#custom_dir) 테마를 사용하여 라이브러리를 포함하여 더 나은  결과를 얻을 수 있다.

## custom_dir 테마 사용 <a id="using-the-theme-custom_dir"></a>
[theme.custom_dir](../configuration/#custom_dir) 설정 옵션을 사용하여 상위 테마의 파일을 오버라이드하는 파일 디렉터리를 가리킬 수 있다. 상위 테마는 [theme.name](../configuration/#name) 설정 옵션에 정의된 테마이다. <code>custom_dir</code>에 있는 파일이 상위 테마에 있는 파일과 이름이 같으면 상위 테마에서 같은 이름의 파일이 이들로 대체된다. <code>custom_dir</code>의 모든 파일이 상위 테마에 추가된다. <code>custom_dir</code>의 내용은 상위 테마의 디렉터리 구조를 미러링해야 한다. 템플릿, JavaScript 파일, CSS 파일, 이미지, 글꼴 또는 테마에 포함된 다른 미디어를 포함할 수 있다.

> Note <br>
> 이 설정을 작동하려면 <code>theme.name</code> 설정을 설치된 테마로 설정해야 한다. 대신 이름 설정이 <code>null</code>로 설정되어 있거나 정의되지 않은 경우 d오버라이드할 테마는 없으며 <code>custom_dir</code>의 내용은 독립적으로 실행될 수 있는 완전한 테마여야 한다. 자세한 내용은 [테마 개발자 가이드](https://www.mkdocs.org/dev-guide/themes/)에서 찾을 수 있다.

예를 들어 [mkdocs]../choosing-your-theme/#mkdocs) 테마([browse source](https://github.com/mkdocs/mkdocs/tree/master/mkdocs/themes/mkdocs))는 다음과 같은 (부분적으로) 디렉토리 구조를 포함하고 있다.

```yaml
- css\
- fonts\
- img\
  - favicon.ico
  - grid.png
- js\
- 404.html
- base.html
- content.html
- nav-sub.html
- nav.html
- toc.html
```

해당 테마에 포함된 파일을 오버라이드하려면 <code>docs_dir</code> 옆에 새 디렉터리를 만들어야 한다.

```shell
mkdir custom_theme
```

그런 다음 <code>mkdocs.yml</code> 설정 파일에서 새 디렉토리를 지정하여야 한다.

```yaml
theme:
    name: mkdocs
    custom_dir: custom_theme/
```

404 오류 페이지("file not found")를 오버라이드하려면 <code>custom_theme</code> 디렉토리에 <code>404.html</code>이라는 새 템플릿 파일을 추가한다. 템플릿에 포함할 수 있는 항목에 대한 자세한 내용을 [Theme Developer Guide](https://www.mkdocs.org/dev-guide/themes/)에서 설명하고 있다.

favicon을 오버라이드하려면 <code>custom_theme/img/favicon.ico</code>에서 새 아이콘 파일을 추가하여야 한다.

JavaScript 라이브러리를 포함하려면 라이브러리를 <code>custom_theme/js/</code> 디렉토리에 복사하여야 한다.

디렉토리 구조는 다음과 같다.

```yaml
- docs/
  - index.html
- custom_theme/
  - img/
    - favicon.ico
  - js/
    - somelib.js
  - 404.html
- config.yml
```

> Note <br>
> (<code>name</code>으로 정의된) 상위 테마에는 포함되지만 <code>custom_dir</code>에는 포함되지 않은 파일은 계속 사용될 수 있다. <code>custom_dir</code>은 상위 테마의 파일만 오버라이드 또는 대체된다. 파일을 제거하거나 테마를 처음부터 작성하려면 [Theme Developer Guide](https://www.mkdocs.org/dev-guide/themes/)를 읽어 보아야 한다.

### 템플릿 블럭 오버라이딩 <a id="overriding-template-blocks"></a>
기본 테마는 <code>main.html</code> 템플릿에서 개별적으로 오버라이드할 수 있는 템플릿 블록 내부에 많은 부분을 구현하였다. <code>custom_dir</code>에 <code>main.html</code> 템플릿 파일을 만들고 해당 파일 내에 교체 블록을 정의하기만 하면 된다. <code>main.html</code>이 <code>base.html</code>를 확장한다. 예를 들어, MkDocs 테마 제목을 변경하려면 다음을 포함하도록 <code>main.html</code> 템플릿를 변경하여야 한다.

```html
{% extends "base.html" %}

{% block htmltitle %}
<title>Custom title goes here</title>
{% endblock %}
```

위의 예에서 사용자 정의 <code>main.html</code> 파일에 정의된 <code>htmltitle</code> 블록을 상위 테마에 정의된 기본 <code>htmltitle</code> 블록 대신 사용한다. 상위에 블록이 정의되어 있다면 원하는 만큼 블록을 다시 정의할 수 있다 . 예를 들어, Google Analytics 스크립트를 다른 서비스의 스크립트로 바꾸거나 검색 기능을 사용자의 스크립트로 바꿀 수 있다. 오버라이드할 수 있는 블록을 결정하려면 사용 중인 상위 테마를 참조해야 한다. MkDocs 및 ReadTheDocs 테마는 다음 블록을 제공한다.

- <code>site-meta</code>: 문서 헤드내의 메타 태그를 포함한다.
- <code>htmltitle</code>: 문서 헤드내의 페이지 제목을 포함한다.
- <code>styles</code>: stylesheets를 위한 링크 태그를 포함한다.
- <code>libs</code>: 페이지 헤더에 포함된 (jQuery를 포함한) JavaScript 라이브러리를 포함한다.
- <code>scripts</code>: 페이지를 로드한 다음 수행하는 JavaScript 스크립트를 포함한다.
- <code>analytics</code>: 분석 스크립트를 포함한다.
- <code>extrahead</code>: 커스텀 tags/scripts/etc를 삽입할 <code>\<head\></code>내의 빈 블록
- <code>site_name</code>: 내비게이션 바에 나타나는 site name을 포함한다.
- <code>site_nav</code>: 내비게이션 바에 나타나는 site navigation을 포함한다.
- <code>search_button</code>: 내비게이션 바에 나타나는 탐색 상자를 포함한다.
- <code>next_prev</code>: 내비게이션 바에 나타나는 next와 previous 버튼을 포함한다.
- <code>repo</code>: 내비게이션 바에 나타나는 리포지토리 링크를 포함한다.
- <code>content</code>: 내비게이션 바에 나타나는 페이지 콘텐츠와 목차를 포함한다.
- <code>footer</code>: 페이지 꼬리말을 포함한다.

수정 사항이 사이트의 구조에 적용돌 수 있도록 소스 템플릿 파일을 볼 수도 있다. 사용자 정의 블럭 내에서 사용할 수 있는 변수 리스트는 [Template Variables](https://www.mkdocs.org/dev-guide/themes/#template-variables)를 참조한다. [Jinja 문서](http://jinja.pocoo.org/docs/dev/templates/#template-inheritance)에서 를 블록에 대한 자세한 설명을 참조할 수 있다.

### custom_dir과 템플릿 블럭의 결합 <a id="combining-the-custom_dir-and-template-blocks"></a>
JavaScript 라이브러리를 <code>custom_dir</code>에 추가하면 사용할 수 있지만 MkDocs에서 생성한 페이지에는 포함되지 않는다. 따라서 HTML에 라이브러리 링크를 추가해야 한다.

위의 디렉터리 구조로 시작한다 (생략돰):

```yml
- docs/
- custom_theme/
  - js/
    - somelib.js
- config.yml
```

<code>custom_theme/js/somelib.js</code> 파일에 대한 링크를 템플릿에 추가해야 한다. <code>somelib.js</code>는 JavaScripts 라이브러리이기 때문에 논리적으로 <code>libs</code> 블록에 있어야 한다. 그러나 새 스크립트만 포함하는 새 <code>libs</code> 블록은 상위 템플릿에 정의된 블록을 대체하고 상위 템플릿의 라이브러리에 대한 모든 링크는 제거된다. 템플릿이 파손되는 것을 방지하기 위해 블록 내에서 <code>super</code>에 대한 호출과 함께 [super block](http://jinja.pocoo.org/docs/dev/templates/#super-blocks)을 사용할 수 있다.

```html
{% extends "base.html" %}

{% block libs %}
    {{ super() }}
    <script src="{{ base_url }}/js/somelib.js"></script>
{% endblock %}
```

링크가 항상 현재 페이지에 상대적인지 확인하는 데 [base_url](https://www.mkdocs.org/dev-guide/themes/#base_url) 템플릿 변수를 사용한다.

이제 생성된 페이지에는 라이브러리 뿐만 아니라 <code>custom_dir</code>에 있는 라이브러리기 제공하는 템플릿에 대한 링크를 갖는다. <code>custom_dir</code>에 포함된 추가 CSS 파일도 마찬가지이다.
